###How to run the app
clone the repo and run this command `docker-compose up -d --build`
**kindly check docker-compose.yml** file

####Access web app from `http://${SERVER_IP}`

###Project Structure
1. [LIVE DEMO](#live_demo )
2. [Backend](#backend )
3. [Frontend](#frontend)
4. [Database Migration](#database-migration)
5. [nginx](#nginx)
6. [Rabbitmq](#rabbitmq)
7. [Continuous integration](#continuous-integration)


### LIVE DEMO
######[LIVE DEMO FRONTEND](http://35.190.155.16/)
######[LIVE DEMO RABBITMQ](http://35.190.155.16:15672)
`username:rabbitmq,password:rabbitmq`
######[LIVE DEMO JENKINS](http://35.190.155.16:8080)   
`username:ahmed,password:admin`






###Backend *Node 10.1*
Under **backend** folder you will find **expressjs** project using [sequelize](http://docs.sequelizejs.com/) ORM &  RESTFUL style
1. Endpoint `HTTP POST METHOD` `http://${SERVER_IP}:3000/tasks` retrieve tasks according to query object request body
ex : `{"page":1,"query":{"sort":"status","filter":{"status":"pending","courier":"mark"}}}`

2. Endpoint `HTTP PUT METHOD` `http://${SERVER_IP}:3000/tasks/:task_id` to update task status and publish rabbitMQ message , request body ex : `{"status":"pending"}` update tasks status and **log into loggers table in database**


##### Backend Structure
1. index.js *App Entry Point*
2. routes folder *controllers*
3. models folder *database and business files*
4. helper folder *contain helper middleware to handler rabbitmq pub/subs to save into logger table in database*

##### you can check **backend/Dockerfile** to learn how to run endpoints in docker application container


###Frontend *Angular 5*

####Angular Project Structure
1. **Dynamic List Component** under **/src/app/factories/list** to generate dynamic list and pagination using JSON input ex:`this.config = {dataSource:"http://api_uri"}`

2. **Dynamic Form Component** under **/src/app/factories/list** to generate dynamic forms

3. **Modules** folder to enhance modularity

4. **providers** folder for global services

5. **models** folder ... holds components configurations
#####LIVE DEMO [WIMO_TASK](http://35.190.155.16)

###Database Migration
Under **database** folder you can put sql files and push to repo after that bitbucket web hook will run docker-compose up --build to migrate database.

1. tasks table , holds tasks
2. loggers table , holds transaction for updating task status

#####LIVE DEMO `mysql -h 35.190.155.16 -u root -p`
`password:12345`

###nginx
Under **nginx** folder , web server to handle HTTP request for angular app and express app
make proxey for backend and frontend kindly check **nginx/default.conf** file
**make sure that port 80 not in use**

###Rabbitmq
RabbitMQ is the most widely deployed open source message broker
you will find Rabbitmq configurations in **docker-compose.yml** and you can access it after docker-compose up from `http://${SERVER_IP}:15672`
####LIVE DEMO [MyRabbitmq](http://35.190.155.16:15672/)
**UserName:rabbitmq**
**Password:rabbitmq**


###Continuous integration
Using Jenkins and bitbucket web hook , once you push commit , web hook run jenkins job to `docker-compose up --build`
LIVE DEMO [WIMOTASK Jenkins](http://35.190.155.16:8080/)
**UserName:ahmed **
**Password:admin **
