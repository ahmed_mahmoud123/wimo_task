'use strict';
var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(__filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require(path.join(__dirname,'..','config','config.js'))[env];
var db        = {};

var sequelize = new Sequelize(config.db_name, config.db_username, config.db_password,config.sequelize_config);

fs.readdirSync(__dirname)
.filter(file => {
  return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
})
.forEach(file => {
  var model = sequelize['import'](path.join(__dirname, file));
  db[model.name] = model;
});


db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
