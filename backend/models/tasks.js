'use strict';
var amqp = require('amqplib/callback_api');

module.exports = (sequelize, DataTypes) => {
  var Task = sequelize.define('tasks', {
    fromLocation: DataTypes.STRING,
    toLocation: DataTypes.STRING,
    deliveryDate:DataTypes.DATE,
    startedAt:DataTypes.STRING,
    finishedAt:DataTypes.STRING,
    driverName:DataTypes.STRING,
    courier:DataTypes.STRING,
    description:DataTypes.TEXT,
    status:DataTypes.ENUM('started', 'pending', 'completed', 'failed')
  });


  Task.updateStatus =(id,status,rabbitmqChannel)=>{
    rabbitmqChannel.sendToQueue(process.env.RABBITMQ_CHANNEL_NAME, Buffer.from(`Task with id ${id} was updated to be ${status} status`));
    return Task.update({ status:status },{ where:{ id : id },silent: true }).then(result=>{
      if(result.length > 0 )
        return Task.findById(id,{ attributes: { exclude: ['createdAt','updatedAt'] } });
    });
  }
  Task.paginate = (config = { page:1,limit:5 } )=>{
    let offset = config.limit * (config.page - 1);
    let queryOptions = {
      attributes: { exclude: ['createdAt','updatedAt'] },
      limit: config.limit,
      offset: offset,
    }
    if(config.query && config.query.sort){
      queryOptions.order = [[config.query.sort,'ASC']];
    }
    if(config.query && config.query.filter){
      const Op = sequelize.Op;
      let conditions = {};
      for (let condition in config.query.filter) {
        if(!config.query.filter[condition]){
          config.query.filter[condition] = '';
        }
        conditions[condition] = { [Op.like]: `%${config.query.filter[condition]}%` }
      }
      queryOptions.where = conditions;
    }
    return Task.findAndCountAll(queryOptions);
  }
  return Task;
};
