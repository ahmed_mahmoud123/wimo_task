'use strict';
var amqp = require('amqplib/callback_api');

module.exports = (sequelize, DataTypes) => {

  var Logger = sequelize.define('loggers', {
    transaction:DataTypes.STRING
  });

  Logger.saveToLog =(transaction)=>{
    return Logger.create({ transaction:transaction },{silent: true }).then(result=>{
      console.log(result);
    });
  }

  return Logger;
};
