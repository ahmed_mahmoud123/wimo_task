process.env.RABBITMQ_CHANNEL_NAME = "updateTaskStatus";

module.exports = {
  "production":{
    db_username:"",
    db_password:"",
    db_host:"",
    db_name:""
  },
  "development":{
    db_username:process.env.MYSQL_USER,
    db_password:process.env.MYSQL_PASSWORD,
    db_host:"docker-mysql",
    db_name:process.env.MYSQL_DATABASE,
    sequelize_config:{
      host: 'docker-mysql',
      dialect: 'mysql',
      operatorsAliases: false,
      pool: {
        port:3306,
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
    }
  }
}
