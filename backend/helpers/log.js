var amqp = require('amqplib/callback_api');
var models = require('../models');

var connection,rabbitmqChannel;

module.exports = ( req , resp , next ) => {
  if(connection){
    req.rabbitmqChannel = rabbitmqChannel;
    next();
  }else{
    amqp.connect(`amqp://${process.env.RABBITMQ_DEFAULT_USER}:${process.env.RABBITMQ_DEFAULT_PASS}@rabbit1`, function(err, conn) {
        connection = conn;
        conn.createChannel(function(err, ch) {
          ch.assertQueue(process.env.RABBITMQ_CHANNEL_NAME, {durable: false});
          rabbitmqChannel = ch;
          req.rabbitmqChannel = rabbitmqChannel;
          ch.consume(process.env.RABBITMQ_CHANNEL_NAME, function(msg) {
            //console.log(" [x] Received %s", msg.content.toString());
            models.loggers.saveToLog(msg.content.toString());
          }, {noAck: true});
          next();
        });
    });
  }
};
