'use strict';

var express = require('express');
var models = require('../models');
var bodyParser = require('body-parser');
var loggerMidd = require('../helpers/log');

var JSONParserMid = bodyParser.json();
var router = express();

router.post('/',JSONParserMid,(req,resp)=>{
  console.log(req.body);
  let page = parseInt((req.body.page && req.body.page > 0 ) ? req.body.page : 1);
  let config = { page ,limit:5 , query: req.body.query };
  models.tasks.paginate(config).then((data) => {
    let pages = Math.ceil(data.count / config.limit);
    resp.status(200).json({'rows': data.rows, 'count': data.count, 'pages': pages, 'page': config.page });
  })
  .catch(function (error) {
		resp.status(500).json(error);
	});
});

router.put('/:id',[ JSONParserMid , loggerMidd ],(req,resp)=>{
  models.tasks.updateStatus(req.params.id,req.body.status,req.rabbitmqChannel).then((result) => {
    resp.status(200).json(result);
  })
  .catch(function (error) {
		resp.status(500).json(error);
	});
});

module.exports = router;
