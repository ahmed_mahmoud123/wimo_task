var fs = require('fs');
var path = require('path');
var express = require('express');
var app = express();

app.use((req,resp,next)=>{
  resp.header('Access-Control-Allow-Methods','GET,PUT,POST');
  resp.header('Access-Control-Allow-Headers','Content-Type');
  resp.header('Access-Control-Allow-Origin','*');
  next();
});

const tasksRoutes = require('./routes/tasks');
app.use('/tasks',tasksRoutes);

app.listen(3000);
