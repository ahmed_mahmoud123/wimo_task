-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: wimo
-- ------------------------------------------------------
-- Server version	5.7.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `loggers`
--

DROP TABLE IF EXISTS `loggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loggers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction` varchar(255) DEFAULT NULL,
  `createdAt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loggers`
--

LOCK TABLES `loggers` WRITE;
/*!40000 ALTER TABLE `loggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `loggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromLocation` varchar(255) NOT NULL,
  `toLocation` varchar(255) NOT NULL,
  `deliveryDate` date NOT NULL,
  `startedAt` varchar(255) NOT NULL,
  `finishedAt` varchar(255) NOT NULL,
  `driverName` varchar(255) NOT NULL,
  `courier` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('completed','failed','pending','started') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (21,'25.204849,55.270783','25.125386, 55.227821','2019-05-10','2019-05-10 01:36:08','2019-05-10 01:56:09','Marko Pandres','FastWay','Deliver a credit card, user must sign','failed'),(22,'25.191099, 55.283402','25.127795, 55.226619','2019-05-10','2019-05-10 04:26:08','2019-05-10 04:56:00','Anmol Dares','Wimo','Deliver a bank statement','failed'),(23,'25.194594, 55.274034','25.138623, 55.231355','2019-05-10','2019-05-10 09:21:18','2019-05-10 09:43:00','Marko Pandres','FastWay','Deliver souq.com order','completed'),(24,'25.166517, 55.278027','25.089483, 55.189321','2019-05-10','2019-05-10 09:16:38','2019-05-10 04:33:10','Adam Aldo','Wimo','Grocery Delivery','completed'),(25,'25.166517, 55.278027','25.074626, 55.193905','2019-05-10','2019-05-10 11:23:03','2019-05-10 04:44:40','Marko Pandres','FastWay','Deliver a credit card, user must sign','completed'),(26,'25.166051, 55.271847','25.074626, 55.193905','2019-05-10','2019-05-10 11:26:08','2019-05-10 11:56:00','Adam Aldo','Wimo','Deliver noon.com shipping','pending'),(27,'25.194594, 55.274034','25.089483, 55.189321','2019-05-10','2019-05-10 15:56:28','2019-05-10 16:32:40','Anmol Dares','Wimo','Deliver a document shipping','pending'),(28,'25.089240, 55.211242','25.138623, 55.231355','2019-05-22','','','Nazih Omar','FastWay','Deliver emirates ID','pending'),(29,'25.166051, 55.271847','25.127795, 55.226619','2019-05-12','2019-05-12 13:06:08','','Marko Pandres','FastWay','Deliver emirates ID','pending'),(30,'25.194594, 55.274034','25.125386, 55.227821','2019-05-25','','','Adam Aldo','Wimo','Deliver a souq.com shipping','failed');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-22 11:10:40
