import { ListItemComponent } from '../../modules/tasks/list-item/list-item.component';
export class ListTasksModel{

  public config:any;
  public filteration_config;
  constructor(filter_fn,self){
    this.config = {
      dataSource:"/tasks",
      list_item:ListItemComponent
    }
    this.filteration_config = {
      props:{

      },
      rows:[
        [
          {type:'textfield',col:4,label:'Driver Name',name:'driverName'},
          {type:'textfield',col:4,label:'Courrier',name:'courier'},
          {type:'select',col:4,placeholder:"Filter By Tasks Status",label:'Status',name:'status',options:[{text:"All",value:""},"pending","completed","failed","started"]}
        ],
        [
          {type:'button',col:3,offset:9,value:'filter',custom:true,fn:filter_fn,self:self,icon:'search'}
        ]
      ]
    }
  }
}
