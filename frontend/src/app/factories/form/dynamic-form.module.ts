import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { APIService } from '../../providers/api.service';
import { DynamicField } from './dynamic-element.derictive';
import { DynamicFormComponent } from './dynamic-form.component';
import { ButtonComponent } from './elements/button/button.component';
import { HttpClientModule } from '@angular/common/http';
import { ValidationComponent } from './elements/validation/validation.component';
import { TextFieldComponent } from './elements/textfield/textfield.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SelectComponent } from './elements/select/select.component';




@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,

    BrowserAnimationsModule,

  ],
  declarations: [
    DynamicFormComponent,
    DynamicField,
    TextFieldComponent,
    ButtonComponent,
    SelectComponent,
    ValidationComponent,
  ],
  exports: [
    DynamicFormComponent,
  ],
  entryComponents:[
    TextFieldComponent,
    ButtonComponent,
    SelectComponent
  ],providers:[APIService]
})

export class DynamicFormModule { }
