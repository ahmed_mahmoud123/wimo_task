import { Component,Input,Output,OnInit,EventEmitter} from '@angular/core';
import { FormGroup , Validators ,FormBuilder ,ValidatorFn, FormControl , FormArray } from '@angular/forms';
import { APIService } from '../../providers/api.service';

const validations = {
  'required' : Validators.required,
}

@Component({
  selector : "dynamic-form",
  templateUrl:"./generic-form.component.html",
  styleUrls:['./generic-form.component.css']
})

export class DynamicFormComponent implements OnInit {
  group:any;
  @Input() config : any = []; /* {props:{action:""} ,elements:[{type:"input",name:"first_name"}] } */
  @Output() afterSubmited : any = new EventEmitter(); /*fire when response back from the server */
  @Output() groupCreated : any = new EventEmitter(); /*fire when form group created */
  working:boolean= false;
  /* for 'config' variable structure kindly check models folder */
  /*Dynamic Form Component to build form depend on 'config' variable which has structure like {props:{},elements:[]}  */
  constructor(private apiService:APIService,private formBuilder:FormBuilder){

  }

  ngOnInit(){
    this.createGroup();
  }

  /* Build Form group using form elements form 'config' variable */
  private createGroup():void {
    this.group = this.formBuilder.group({});
    // get elements from 'config' variable and add it to form group and each element may has validators
    this.config.rows.forEach(row=>{
      row.forEach(element=>{
        if(element.name)
          this.group.addControl(element.name,this.formBuilder.control(element.value,Validators.compose(this.buildValidatorsArray(element.validators))));
      });
      });
    this.groupCreated.emit({group:this.group,self:this});
  }




  submit(e){
    if(this.group.valid){

    }
  }

  // build validators array for certain control
  buildValidatorsArray(rules):Array<ValidatorFn>{
    let validators:Array<ValidatorFn> = [];
    if(rules){
      rules.forEach(rule=>{
        validators.push(validations[rule]);
      });
    }
    return validators;
  }



}
