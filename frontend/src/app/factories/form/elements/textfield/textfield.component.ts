import { Component } from '@angular/core';
import { BasicElement } from '../basic-element.component';

@Component({
  templateUrl:'./textfield.component.html',
  styles:["textfield.component.css"]
})

export class TextFieldComponent extends BasicElement{

}
