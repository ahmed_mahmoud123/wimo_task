import { Component } from '@angular/core';
import { BasicElement } from '../basic-element.component';

@Component({
  templateUrl:'./button.component.html',
  styles:["button.component.css"]
})

export class ButtonComponent extends BasicElement{

  click(evt,btn){
    if(btn.custom){
      evt.preventDefault();
      btn.fn.bind(btn.self)(this.group);
    }
  }
}
