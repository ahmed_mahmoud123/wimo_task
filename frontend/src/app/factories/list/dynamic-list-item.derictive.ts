import { ComponentFactoryResolver,OnInit,Input,Directive,ViewContainerRef } from '@angular/core';


@Directive({
  selector : "[dynamicListItem]"
})


export class DynamicListItem implements OnInit{
  @Input() config;
  @Input() row;
  /* Directive to generate component in runtime for List Item */
  constructor(private resolver:ComponentFactoryResolver,private container:ViewContainerRef){

  }

  ngOnInit(){
    // Create Instance from certain component at runtime
    const factory = this.resolver.resolveComponentFactory<any>(this.config.list_item);
    let component = this.container.createComponent(factory);
    component.instance.row = this.row ;
  }
}
