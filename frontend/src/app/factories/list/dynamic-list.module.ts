import { NgModule } from '@angular/core';
import { DynamicListComponent } from './dynamic-list.component';
import { CommonModule } from '@angular/common';
import { APIService } from '../../providers/api.service';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DynamicListItem } from './dynamic-list-item.derictive';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,

  ],
  declarations: [
    DynamicListComponent,
    DynamicListItem
  ],
  exports: [
    DynamicListComponent
  ],
  entryComponents:[

  ],
  providers:[APIService]
})

export class DynamicListModule { }
