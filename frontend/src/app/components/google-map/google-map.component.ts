import { Component, OnInit , Input } from '@angular/core';
declare var google:any;

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})
export class GoogleMapComponent implements OnInit {

  @Input() mapId;
  @Input() set coords(coords: any) {
    if(coords)
      this.initMap(coords);
  }
  constructor() { }

  ngOnInit() {

  }

  initMap(coords) {
   var map = new google.maps.Map(document.getElementById('map'+this.mapId), {
     zoom: 10,
     center: {lat: coords[0].lat, lng: coords[0].lng},
     mapTypeId: 'terrain'
   });
   var marker = new google.maps.Marker({
      position: coords[0],
      map: map,
      title: 'Start Location'
    });

    var marker = new google.maps.Marker({
       position: coords[1],
       map: map,
       title: 'End Location'
     });


   var flightPlanCoordinates = coords;
   var flightPath = new google.maps.Polyline({
     path: flightPlanCoordinates,
     geodesic: true,
     strokeColor: '#FF0000',
     strokeOpacity: 1.0,
     strokeWeight: 2
   });

   flightPath.setMap(map);
 }

}
