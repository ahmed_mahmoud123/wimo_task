import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable()
export class APIService {
  headers:Headers;
  api_url:string;

  constructor(public http: HttpClient) {
    this.api_url="/api";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }



  get(urn){
    return this.http.get(`${this.api_url}${urn}`).toPromise();
  }

  post(urn,obj){
    return this.http.post(`${this.api_url}${urn}`,obj,httpOptions).toPromise();
  }

  put(urn,obj){
    return this.http.put(`${this.api_url}${urn}`,obj,httpOptions).toPromise();
  }



}
