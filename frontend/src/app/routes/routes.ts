import {  Routes } from '@angular/router';
import { ListComponent } from '../modules/tasks/list/list.component';



export class RoutesHelper{

  static routes:Routes=[
    {
      path: "tasks",
      component: ListComponent,
      data: { title: 'Tasks'}
    }
    ,
    {
      path: '',
      redirectTo: '/tasks',
      pathMatch: 'full'
    },
    { path: '**', component: ListComponent }
  ]
}
