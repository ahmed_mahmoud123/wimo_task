import { Component, OnInit,Input } from '@angular/core';
declare var M:any;
import { APIService } from '../../../providers/api.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {

  row:any = {};
  coords:Object;
  constructor(private apiService:APIService){

  }
  ngOnInit(){

  }
  ngAfterViewInit(){
    var elemsModel = document.querySelectorAll('.modal');
    M.Modal.init(elemsModel);
  }


  setTaskCoords(from,to){
    this.coords = [
      this.formateLocation(from),
      this.formateLocation(to),
    ]
  }

  formateLocation(address){
    address = address.split(',');
    return {lat: parseFloat(address[0].trim()), lng: parseFloat(address[1].trim())}
  }

  update_status(evt , value , id){
    if(confirm('are you sure ?')){
      this.apiService.put(`/tasks/${id}`,{
        status:value
      }).then(row=>{
        this.row = row;
      });
    }else{
      evt.target.checked = false;
    }
  }

}
