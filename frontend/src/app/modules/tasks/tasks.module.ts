import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { FormsModule } from '@angular/forms';
import { DynamicListModule } from '../../factories/list/dynamic-list.module';
import { DynamicFormModule } from '../../factories/form/dynamic-form.module';
import { GoogleMapComponent } from '../../components/google-map/google-map.component';
import { APIService } from '../../providers/api.service';
@NgModule({
  imports: [
    CommonModule,
    DynamicListModule,
    DynamicFormModule,
    FormsModule
  ],
  declarations: [
    ListComponent,
    GoogleMapComponent,
    ListItemComponent
  ],
  entryComponents:[
    ListItemComponent
  ],
  providers:[APIService]
})
export class TasksModule { }
