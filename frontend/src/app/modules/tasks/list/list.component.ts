import { Component, OnInit } from '@angular/core';
import { ListTasksModel } from '../../../models/tasks/list';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  listConfig:Object;
  queryObject:any = {};
  filteration_config:Object = {};
  sort_critieria:string;
  filter_critieria:Object;
  constructor() {
    let model = new ListTasksModel(this.filter,this);
    this.listConfig = model.config;
    this.filteration_config = model.filteration_config;
  }

  ngOnInit() {
  }

  filter(group){
    this.filter_critieria = group.value;
    this.build_query_object();
  }
  sort(value){
    this.build_query_object();
  }

  build_query_object(){
    this.queryObject={
      sort:this.sort_critieria,
      filter:this.filter_critieria
    }
  }

}
